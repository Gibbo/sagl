# Getting Started#

1. Download the source code
2. Create a new XNA Project like you would normally
3. In the solution explorer, right click your solution and go to "Add..." then "Existing project"
4. Locate the downloaded source and double click the .sln file
5. In your XNA Game1.cs, change the inherited type to Sagl.Core.Game
6. Remove Initialize() from your Game1 class

Congrats! You have now imported the source into your XNA game and can start using the library.

Super Awesome Game Library (SAGL) is a wrapper to the final version of XNA Game Studio before Microsoft decided to bin it. It expands in functionality by adding and handling input, screen and rendering states. It's fairly simple to use, and well commented.
However due to XNA being dead, I would recommend you just use MONOGame or whatever it is they use these days.