using Microsoft.Xna.Framework;

namespace Sagl.Math
{
    /// <summary>
    /// Holds the position, rotation and scale properties
    /// </summary>
    public class Transform2D
    {
        // Static helpers
        public static Quaternion ZERO_ROTATION = Quaternion.Identity;
        public static Matrix ZERO_TRANSFORM = Matrix.Identity;
        public static Vector3 VECTOR3_ZERO = new Vector3(0, 0, 0);
        public static Vector3 VECTOR3_ONE = new Vector3(1, 1, 1);
        public static Vector2 VECTOR2_ZERO = new Vector2(0, 0);
        public static Vector2 VECTOR2_ONE = new Vector2(1, 1);

        // Flag to determine if this transforms position should be relative to the parent
        public const byte P_POS = 1;
        // Flag to determine if this transforms rotation should be relative to the parent
        public const byte P_ROT = 2;
        // Flag to determine if this transforms scale should be relative to the parent
        public const byte P_SCALE = 4;
        // This flag is used if this transform is relative to the parent with position, rotation and scale
        public const byte P_ALL = (P_POS | P_ROT | P_SCALE);
        // The flags to be used for this transforms parent
        public byte p_flag = P_ALL;
        
        // The transform to be child do
        protected Transform2D parent;
        public Vector3 position = new Vector3();
        public Vector3 scale = new Vector3(1, 1, 1);
        public Quaternion rotation = Quaternion.Identity;
        public Vector2 origin = new Vector2();
        // If the transform should inherit from the parent, this is used in numerous methods
        bool inheritParent;

        // Scale and rotation matrix
        public Matrix model = new Matrix();
        // Position matrix
        public Matrix transform = Matrix.Identity;

        // Temp vectors and matrices to prevent GC crazyness
        private Vector2 vec2tmp;
        private Vector3 vec3tmp;
        private Quaternion quattmp;
        private Matrix mattmp;

        /// <summary>
        /// Sets the parent of this transform, this allows this transform to be transformed relative to the parent
        /// </summary>
        /// <param name="transform"></param>
        public void SetParent(Transform2D transform)
        {
            parent = transform;
        }

        /// <summary>
        /// Gets the transform of the parent
        /// </summary>
        /// <returns>if there is no parent a identity transform is returned</returns>
        public Matrix GetParentTransform()
        {
            return parent != null ? parent.GetTransform() : ZERO_TRANSFORM;
        }

        /// <summary>
        /// Gets the rotation of the parent
        /// </summary>
        /// <returns>If there is no parent it returns a zero rotation</returns>
        public Quaternion GetParentRotation()
        {
            return parent != null ? parent.GetRotation() : ZERO_ROTATION;
        }

        /// <summary>
        /// Gets the parent scale
        /// </summary>
        /// <returns>If there is no parent it returns a vector2 one</returns>
        public Vector2 GetParentScale()
        {
            return parent != null ? parent.GetScale() : VECTOR2_ONE;
        }

        /// <summary>
        /// Gets the model view matrix, this is the scale and rotation of the transform
        /// </summary>
        /// <returns></returns>
        public Matrix GetModelView()
        {
            model = Matrix.Identity * Matrix.CreateScale(scale);
            return model;
        }

        /// <summary>
        /// Gets the transform in the form of a matrix4x4
        /// </summary>
        /// <returns></returns>
        public Matrix GetTransform()
        {
            inheritParent = ((p_flag & P_POS) == P_POS);
            transform = GetModelView() * (inheritParent ? GetParentTransform() : ZERO_TRANSFORM) * Matrix.CreateTranslation(position);
            return transform;
        }

        /// <summary>
        /// Sets the position of this transform
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetPosition(float x, float y)
        {
            position.X = x;
            position.Y = y;
        }

        public void SetPosition(Vector2 pos)
        {
            SetPosition(pos.X, pos.Y);
        }

        public void Translate(float x, float y)
        {
            position.X += x;
            position.Y += y;
        }

        /// <summary>
        /// Sets the scale of this transform
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetScale(float x, float y)
        {
            scale.X = x;
            scale.Y = y;
            scale.Z = 1;
        }

        /// <summary>
        /// Scales the transform relative to its current scale
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Scale(float x, float y)
        {
            scale.X += x;
            scale.Y += y;
        }

        /// <summary>
        /// Sets the angle of this transform in radians
        /// </summary>
        /// <param name="degrees"></param>
        public void SetAngle(float degrees)
        {
            vec3tmp.X = 0;
            vec3tmp.Y = 0;
            vec3tmp.Z = 1;
            rotation = Quaternion.CreateFromAxisAngle(vec3tmp, degrees);
        }

        /// <summary>
        /// Gets the position in local coordinates (not relative to the parent)
        /// </summary>
        /// <returns></returns>
        public Vector2 GetLocalPosition()
        {
            vec2tmp.X = position.X;
            vec2tmp.Y = position.Y;
            return vec2tmp;
        }

        /// <summary>
        /// Gets the position
        /// </summary>
        /// <returns></returns>
        public Vector2 GetPosition()
        {
            vec3tmp = GetTransform().Translation;
            vec2tmp.X = vec3tmp.X;
            vec2tmp.Y = vec3tmp.Y;
            return vec2tmp;
        }

        /// <summary>
        /// Gets the rotation as a quaternion
        /// </summary>
        /// <returns></returns>
        public Quaternion GetRotation()
        {
            inheritParent = ((p_flag & P_ROT) == P_ROT);
            return rotation * (inheritParent ? GetParentRotation() : ZERO_ROTATION);
        }

        /// <summary>
        /// Gets the rotation in radians
        /// </summary>
        /// <returns></returns>
        public float GetRotationRad()
        {
            quattmp = GetRotation();
            return (float)(2 * System.Math.Acos(quattmp.W / quattmp.Length()));    
        }

        /// <summary>
        /// Gets the rotation in degrees
        /// </summary>
        /// <returns></returns>
        public float GetRotationDeg()
        {
            return MathHelper.ToDegrees(GetRotationRad());
        }

        /// <summary>
        /// Gets the scale as a vector2
        /// </summary>
        /// <returns></returns>
        public Vector2 GetScale()
        {
            inheritParent = ((p_flag & P_SCALE) == P_SCALE);
            vec2tmp.X = scale.X;
            vec2tmp.Y = scale.Y;
            vec2tmp *= (inheritParent ? GetParentScale() : VECTOR2_ONE);
            return vec2tmp;
        }
    }
}
