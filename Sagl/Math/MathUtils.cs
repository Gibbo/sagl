﻿using System;
using Microsoft.Xna.Framework;

// Author : Stephen Gibson

namespace Sagl.Math
{
    public class MathUtils
    {
        private static Random rnd = new Random();

        /// <summary>
        /// Generates a random number between 0 and the given range
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static int Random(int range)
        {
            return rnd.Next(range + 1);
        }

        /// <summary>
        /// Generates a random int between the specified start and end values
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static int Random(int start, int end)
        {
            return rnd.Next(start, end);
        }

        /// <summary>
        /// Generates a random boolean value
        /// </summary>
        /// <returns></returns>
        public static bool RandomBoolean()
        {
            return rnd.Next(0, 2) == 1;
        }

        /// <summary>
        /// Generates a random float between 0 and the range specified
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static float Random(float range)
        {
            return (float) (rnd.NextDouble() * range);
        }

        /// <summary>
        /// Generates a random float between the specified start and end values
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static float Random(float start, float end)
        {
            return (float) (start + rnd.NextDouble() * (end - start));
        }

        /// <summary>
        /// Checks if a given value is a power of 2
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsPowerOf2(int value)
        {
            return (value != 0 && (value % 2 == 0));
        }

        /// <summary>
        /// Checks for fuzzy equality between floating point positions on a 2D plane
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <param name="tolerence"></param>
        /// <returns></returns>
        public static bool FuzzyEpsilonEquals(float v1, float v2, float tolerence)
        {
            return !(System.Math.Abs(v2 - v1) > tolerence);
        }

        /// <summary>
        /// Keeps a given value between the specified min and max values
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float Clamp(float min, float max, ref float value)
        {
            return (value < min ? (value = 0) : value > max ? (value = max) : value);
        }

        public static float RatioCalculator(float oWidth, float oHeight, float newWidth)
        {
            return oWidth / oHeight * newWidth;
        }

        /// <summary>
        /// Gives the intercept point between a stationary vector and moving vector
        /// </summary>
        /// <param name="aggressor"></param>
        /// <param name="target"></param>
        /// <param name="targetVelocity"></param>
        /// <param name="speed"></param>
        /// <returns></returns>
        public static Vector2 Intercept(Vector2 aggressor, Vector2 target, Vector2 targetVelocity, float speed)
        {
            float vMag = speed;
            // Directional Vector between both object
            Vector2 ab = target - aggressor;

            // Normalize ab
            ab = Vector2.Normalize(ab);

            // Dot product of ab
            float dot = ab.X * targetVelocity.X + ab.Y * targetVelocity.Y;
            float ujx = dot * ab.X;
            float ujy = dot * ab.Y;

            float vix = targetVelocity.X - ujx;
            float viy = targetVelocity.Y - ujy;

            // Calculate mag of vj
            float viMag = (float) System.Math.Sqrt(vix * vix + viy * viy);
            float vjMag = (float) System.Math.Sqrt(vMag * vMag - viMag * viMag);

            // Multiple out to get vj
            float vjx = ab.X * vjMag;
            float vjy = ab.Y * vjMag;

            // Add vj and vi to get v
            return new Vector2(vjx + vix, vjy + viy);
        }

        /// <summary>
        /// Calculates a point along a cubic bezier curve
        /// </summary>
        /// <param name="t"></param>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <returns></returns>
        public Vector2 CalculateBezierPoint(float t, Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
        {
            var u = 1 - t;
            var tt = t * t;
            var uu = u * u;
            var uuu = uu * u;
            var ttt = tt * t;

            var p = new Vector2(uuu) * p0;
            p += 3 * uu * t * p1;
            p += 3 * u * tt * p2;
            p += ttt * p3;
            return p;
        }
    }
}