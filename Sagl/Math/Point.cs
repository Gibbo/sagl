﻿using Microsoft.Xna.Framework;

namespace Sagl.Math
{
    /// <summary>
    /// A coordinate in 2D space on a cartesian plane
    /// </summary>
    public class Point
    {
        // The X and Y coordinates of the point
        private Vector2 point;

        public Point(float x, float y)
        {
            SetPoint(x, y);
        }

        /// <summary>
        /// Sets the point to the X and Y values
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetPoint(float x, float y)
        {
            point = new Vector2(x, y);
        }

        /// <summary>
        /// Gets the point as a vector 2
        /// </summary>
        /// <returns></returns>
        public Vector2 GetPoint()
        {
            return point;
        }

        /// <summary>
        /// Gets the X coordinate of the point
        /// </summary>
        /// <returns></returns>
        public float GetX()
        {
            return point.X;
        }

        /// <summary>
        /// Gets the Y coordinate of the point
        /// </summary>
        /// <returns></returns>
        public float GetY()
        {
            return point.Y;
        }
    }
}
