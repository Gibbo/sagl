﻿using System;

// Author : Stephen Gibson

namespace Sagl.Interfaces
{
    /// <summary>
    /// A observer is an object of some kind that can recieve events from an subject it is registered to.
    /// </summary>
    public interface IObserver
    {
        /// <summary>
        /// This method is called by the subject when it it recieves an event, this allows the observer to
        /// performs some sort of logic
        /// </summary>
        /// <param name="tag">The name of the event or tag</param>
        /// <param name="objects">A list of objects that a subject might need to handle the event</param>
        void OnNotify(string tag, params object[] objects );

    }
}