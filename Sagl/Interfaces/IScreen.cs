﻿using Microsoft.Xna.Framework;

// Author : Stephen Gibson

namespace Sagl.Interfaces
{
    /// <summary>
    /// Represents a screen within the game, such as main menu or options screen. This works as a simple state machine,
    /// where each screen can be treated as a game state.
    /// </summary>
    public interface IScreen
    {
        /// <summary>
        /// Called when the screen should update, this occurs every tick
        /// </summary>
        /// <param name="gameTime">The delta time represented in various values</param>
        void Update(GameTime gameTime);


        /// <summary>
        /// Called when the screen should draw, this occurs every tick
        /// </summary>
        /// <param name="gameTime">The delta time represented in various values</param>
        void Draw(GameTime gameTime);

        /// <summary>
        /// Called whenever the Viewport width/height is changed. This is also called when the screen is changed.
        /// </summary>
        /// <param name="width">Represents the new width of the viewport</param>
        /// <param name="height">Represents the new height of the viewport</param>
        void Resize(int width, int height);

        /// <summary>
        /// Called when the screen is hidden, this will be called before a new screen is set
        /// </summary>
        void Hide();

        /// <summary>
        /// Called automatically when the screen is set
        /// </summary>
        void Show();

        /// <summary>
        /// Called before the Game exits or when the window loses focus
        /// </summary>
        void Pause();

        /// <summary>
        /// Called when the window gains focus
        /// </summary>
        void Resume();
    }
}