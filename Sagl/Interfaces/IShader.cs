﻿// Author : Stephen Gibson

using Sagl.Graphic;

namespace Sagl.Interfaces
{
    /// <summary>
    /// This interface allows objects to return some form of shader effect for the renderer.
    /// </summary>
    public interface IShader
    {

        /// <summary>
        /// Gets the shader effect to be appled to the object
        /// </summary>
        /// <returns>The shader and its data</returns>
        Shader GetShader();

        /// <summary>
        /// Sets the shader that is to be applied to the object
        /// </summary>
        /// <param name="shader"></param>
        void SetShader(Shader shader);


    }
}
