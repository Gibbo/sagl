﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sagl.Core.Input;

// Author : Stephen Gibson

namespace Sagl.Interfaces
{
    public interface IInputProccessor
    {
        /// <summary>
        /// Called every update from within the input handler. 
        /// This allows continious checking for input without state checks.
        /// </summary>
        /// <param name="gameTime"></param>
        void PollInput(GameTime gameTime);

        /// <summary>
        /// Called once when the input handler recieves a key down event
        /// </summary>
        /// <param name="key">The key that has just been pressed</param>
        void KeyDown(Keys key);

        /// <summary>
        /// Called once when the input handler recieves a key up event
        /// </summary>
        /// <param name="key"></param>
        void KeyUp(Keys key);

        /// <summary>
        /// Called when the input handler recieves a mouse pressed event, or a click event whatever.
        /// The corresponding button and X Y coordinate of the mouse will be sent.
        /// </summary>
        /// <param name="button"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void MousePressed(InputHandler.MouseButton button, int x, int y);

        /// <summary>
        /// Called when the input handler recieves a mouse released event
        /// The corresponding button and X Y coordinate of the mouse will be sent.
        /// </summary>
        /// <param name="button"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void MouseReleased(InputHandler.MouseButton button, int x, int y);

        /// <summary>
        /// Called when a mouse button is pressed and then the mouse is dragged
        /// </summary>
        /// <param name="button"></param>
        /// <param name="endX"></param>
        /// <param name="endY"></param>
        void MouseDragged(InputHandler.MouseButton button, int endX, int endY);

        /// <summary>
        /// Called whenever the mouse is moved
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        void MouseMoved(int x, int y);

        /// <summary>
        /// Called whenever the mouse wheel is scrolled
        /// </summary>
        /// <param name="direction">A directio of 1 is scrolling up and -1 is scrolling down</param>
        void Scrolled(int direction);

        void ButtonDown(PlayerIndex index, Buttons button);

        void ButtonUp(PlayerIndex index, Buttons button);
    }
}