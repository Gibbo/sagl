﻿namespace Sagl.Interfaces
{
    /// <summary>
    /// This interface declares the basic methods for disabling, enabling, checking if enabeld
    /// </summary>
    public interface IEnabled
    {
        void SetEnabled(bool state);

        bool IsEnabled();
    }
}
