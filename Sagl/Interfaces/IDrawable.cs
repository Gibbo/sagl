﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// Author : Stephen Gibson

namespace Sagl.Interfaces
{
    /// <summary>
    /// Exposes a public API for drawable objects, objects that implement the drawable interface can
    /// then be used with Renderers
    /// </summary>
    public interface IDrawable : IEnabled
    {
        /// <summary>
        /// Called whenever the drawable instance can be rendered
        /// </summary>
        /// <param name="batch">The batch this drawable will use</param>
        /// <param name="gameTime">The current game time, used for animation</param>
        void Draw(SpriteBatch batch, GameTime gameTime);
    }
}