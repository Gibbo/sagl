﻿using System;
using Microsoft.Xna.Framework;
using Sagl.Core.Input;
using Sagl.Interfaces;

namespace Sagl.Core
{
    /// <summary>
    /// The main class for the library, it is an extended version of XNA.Framework.Game with game state, input and pause/resume functionality.
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        // The current screen that is active
        private IScreen screen;
        protected GraphicsDeviceManager graphics;

        public Game()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferHeight = 640,
                PreferredBackBufferWidth = 480,
            };

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            Window.ClientSizeChanged += OnResize;
            base.Initialize();
            Sagl.game = this;
            Sagl.input = new InputHandler();
            Sagl.graphics = new Graphics(GraphicsDevice);
            Sagl.file = new FileHandle(Content);

        }

        protected override void Update(GameTime gameTime)
        {
            Sagl.time = gameTime;
            if(InputHandler.enabled)
                Sagl.input.Update();
            if (screen != null)
                screen.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            if (screen != null)
                screen.Draw(gameTime);
            base.Draw(gameTime);
        }


        protected override void OnActivated(object sender, EventArgs args)
        {
            // If a screen is active, resume and show it
            if (screen != null)
            {
                screen.Resume();
                screen.Show();
            }
            // Enable input
            InputHandler.enabled = true;
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            // If a screen is active, pause and hide it
            if (screen != null)
            {
                screen.Pause();
                screen.Hide();
            }
            // Disable input
            InputHandler.enabled = false;
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            // When a user shuts the game window down, we call the pause and hide functions of the screen
            if (screen == null) return;
            screen.Pause();
            screen.Hide();
        }

        public void OnResize(object sender, EventArgs args)
        {
            // Call resize on the screen to allow the user to adjust parameters of objects, such as a camera
            if (screen != null)
                screen.Resize(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
        }

        /// <summary>
        /// Sets the game to handle a new screen, this hides the call Hide() on the current
        /// screen and then calls Show() on the new screen
        /// <B>Screens are not cached, when a new screen is set the old reference is lost. You can cache a screen using the <code>GetScreen()</code> method</B>
        /// </summary>
        /// <param name="screen"></param>
        public virtual void SetScreen(IScreen screen)
        {
            if (this.screen != null) this.screen.Hide();
            this.screen = screen;
            this.screen.Show();
            this.screen.Resize(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The currently active screen, if any</returns>
        public IScreen GetScreen()
        {
            return screen;
        }
    }
}