﻿using Microsoft.Xna.Framework.Content;

namespace Sagl.Core
{
    /// <summary>
    /// Handles the loading of assets using the XNA content pipeline, this is simply a small wrapper class for the content manager.
    /// It's one and only function is to get an asset of a given type
    /// </summary>
    public class FileHandle
    {

        private ContentManager content;

        public FileHandle(ContentManager content)
        {
            this.content = content;
        }


        /// <summary>
        /// Gets an asset from the content manager
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public T Get<T>(string path)
        {
            return content.Load<T>(path);
        }
    }
}
