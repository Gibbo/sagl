﻿using Microsoft.Xna.Framework;
using Sagl.Core.Input;

namespace Sagl.Core
{
    /// <summary>
    /// Entry point for the game instance, the graphics device, the input system and other useful shortcuts
    /// </summary>
    public class Sagl
    {
        public static Game game;
        public static Graphics graphics;
        public static InputHandler input;
        public static FileHandle file;
        public static GameTime time;
    }
}
