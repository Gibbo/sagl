﻿using Microsoft.Xna.Framework.Graphics;
using Sagl.Graphic;

namespace Sagl.Core
{
    public class Graphics
    {
        // The current graphics device being used
        private GraphicsDevice _graphics;

        public Graphics(GraphicsDevice graphics)
        {
            _graphics = graphics;
        }

        /// <summary>
        /// Creates a new 2D graphics renderer
        /// </summary>
        /// <returns></returns>
        public Renderer Create2DRenderer()
        {
            return new Renderer(_graphics);
        }

        /// <summary>
        /// Gets the viewport for the application
        /// </summary>
        /// <returns></returns>
        public Viewport GetViewport()
        {
            return _graphics.Viewport;
        }

        /// <summary>
        /// Gets the width of the viewport in pixels
        /// </summary>
        /// <returns></returns>
        public int GetWidth()
        {
            return _graphics.Viewport.Width;
        }

        /// <summary>
        /// Gets the height of the viewport in pixels
        /// </summary>
        /// <returns></returns>
        public int GetHeight()
        {
            return _graphics.Viewport.Height;
        }
    }
}
