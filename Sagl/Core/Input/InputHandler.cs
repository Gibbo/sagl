﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sagl.Interfaces;
using Sagl.Math;

// Author : Stephen Gibson

namespace Sagl.Core.Input
{
    /// <summary>
    /// Handles input and events
    /// </summary>
    public class InputHandler
    {

        // If input is enabled
        public static bool enabled = true;

        /// <summary>
        /// Defines some values for the left, middle and right mouse MouseButton so we can 
        /// pass button events to an InputProccessor
        /// </summary>
        public enum MouseButton
        {
            LEFT = 0,
            MIDDLE = 1,
            RIGHT = 2
        }

        // User defined input proccessor
        private IInputProccessor inputProccessor;

        #region keyboardInput

        // All the keys that are currently pressed
        private Keys[] pressedKeys;
        // The keyboards current state that is used to send key down events
        private KeyboardState keyboardState;
        // This is all the keys that were pressed and that need to be released, used to sent key up events
        private List<Keys> keysToRelease = new List<Keys>();
        // The keyboards previous state that is used to send key up events
        private KeyboardState prevKeyboardState;

        #endregion keyboardInput

        #region mouseInput

        // All the mouse MouseButton, used for iteration to check for down and up events and send the correct button
        private MouseButton[] mouseButton;
        // The mouses current state that is used to send click events
        private MouseState mouseState;
        // The mouses previous state that is used to send button up events
        private MouseState prevMouseState;
        private ButtonState[] mouseButtonStates;
        private ButtonState[] prevMouseButtonStates;

        // Short hand states, to avoid polling the mouse class
        private const ButtonState PRESSED = ButtonState.Pressed;
        private const ButtonState RELEASED = ButtonState.Released;


        // A matrix to transform the mouse coordinates, this is used to conver to world coordinates
        private Matrix transformMatrix = Matrix.Identity;
        // The coordinates of the mouse cursor this update
        private Vector2 mouse;
        // Determines if the mouse is in a dragged state, the mouse can only drag using 1 button at a time
        private bool dragging;
        // The button that is currently down and dragging
        private MouseButton draggingButton;
        // The starting position of the mouse on the X coordinate when in a dragged state
        private Vector2 mouseDragged;

        #endregion mouseInput

        #region padInput

        // Players 1 to 4
        private PlayerIndex[] playerIndexes;
        // All the buttons on the pad, stored for easy access
        private Array buttons;

        private GamePadState[] padStates;
        private GamePadState[] prevPadStates;

        #endregion padInput

        public InputHandler()
        {
            prevKeyboardState = keyboardState;
            prevMouseState = mouseState;

            mouseButtonStates = new ButtonState[3];
            prevMouseButtonStates = new ButtonState[3];
            mouseButton = new MouseButton[3];

            mouseButton [0] = MouseButton.LEFT;
            mouseButton [1] = MouseButton.MIDDLE;
            mouseButton [2] = MouseButton.RIGHT;

            padStates = new GamePadState[4];
            prevPadStates = new GamePadState[4];
            playerIndexes = new PlayerIndex[4];
            playerIndexes [0] = PlayerIndex.One;
            playerIndexes [1] = PlayerIndex.Two;
            playerIndexes [2] = PlayerIndex.Three;
            playerIndexes [3] = PlayerIndex.Four;
            buttons = Enum.GetValues(typeof (Buttons));
        }

        public void Update()
        {
            // If input is disabled, don't run any code
            if(!enabled) return;

            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();
            pressedKeys = keyboardState.GetPressedKeys();
            // Grab the states for each controller that could be connected
            for (int c = 0; c < padStates.Length; c++)
            {
                padStates [c] = GamePad.GetState(playerIndexes [c]);
            }

            // Store the values of the X and Y coordinate of the mouse
            mouse.X = Mouse.GetState().X;
            mouse.Y = Mouse.GetState().Y;

            // Keep the arrays synced with the xna mouse class
            mouseButtonStates [0] = mouseState.LeftButton;
            mouseButtonStates [1] = mouseState.MiddleButton;
            mouseButtonStates [2] = mouseState.RightButton;
            prevMouseButtonStates [0] = prevMouseState.LeftButton;
            prevMouseButtonStates [1] = prevMouseState.MiddleButton;
            prevMouseButtonStates [2] = prevMouseState.RightButton;

            // If the user has set an input processor, we want to send key, mouse and game pad events to it
            if (inputProccessor != null)
            {
                // Check for polled input
                inputProccessor.PollInput(Sagl.time);

                // Check for any keys that have been released since the last update
                for (int x = keysToRelease.Count - 1; x >= 0; x--)
                {
                    Keys tmpKey = keysToRelease [x];
                    if (!prevKeyboardState.IsKeyUp(tmpKey)) continue;
                    inputProccessor.KeyUp(tmpKey);
                    keysToRelease.Remove(tmpKey);
                }

                // Check for any keys that have been pressed since the last update
                foreach (Keys key in pressedKeys)
                {
                    if (!keyboardState.IsKeyDown(key) || !prevKeyboardState.IsKeyUp(key)) continue;
                    inputProccessor.KeyDown(key);
                    keysToRelease.Add(key);
                }

                // Check each button state in the array for press down and
                // release events, then send them to the input proccessor
                for (int x = 0; x < mouseButtonStates.Length; x++)
                {
                    if (mouseButtonStates [x] == PRESSED && prevMouseButtonStates [x] == RELEASED)
                        SendMousePressed(mouseButton [x], mouse.X, mouse.Y);
                    if (mouseButtonStates [x] == RELEASED && prevMouseButtonStates [x] == PRESSED)
                        SendMouseReleased(mouseButton [x], mouse.X, mouse.Y);
                }

                /* Check if the mouse is currently being dragged, this is automatically assumed to be true if a mouse
                 * button is clicked, it only starts sending events if the intitial click down position is different
                 * from the current
                 */
                if (dragging)
                    // Send mouse dragged events to the input proccessor
                    if (!MathUtils.FuzzyEpsilonEquals(mouse.X, mouseDragged.X, 0.0001f) ||
                        !MathUtils.FuzzyEpsilonEquals(mouse.Y, mouseDragged.Y, 0.0001f))
                    {
                        inputProccessor.MouseDragged(draggingButton, (int) mouse.X, (int) mouse.Y);
                    }

                // If the mouses current X or Y is different from the previous, the mouse moved so we send an event
                if (!MathUtils.FuzzyEpsilonEquals(mouse.X, prevMouseState.X, 0.0001f) ||
                    !MathUtils.FuzzyEpsilonEquals(mouse.Y, prevMouseState.Y, 0.0001f))
                {
                    inputProccessor.MouseMoved((int) mouse.X, (int) mouse.Y);
                }

                // Check if the current mouse scroll value is different from the previous
                if (mouseState.ScrollWheelValue != prevMouseState.ScrollWheelValue)
                {
                    // Send an event to the input proccessor -1 means scroll down and 1 means scroll up
                    inputProccessor.Scrolled(mouseState.ScrollWheelValue < prevMouseState.ScrollWheelValue ? -1 : 1);
                }

                /*
                 * Checks for connected and pads and then checks the state of each button.
                 * Where i is the PlayerIndex and b is the button
                 */
                for (int i = 0; i < padStates.Length - 1; i++)
                {
                    // If the controller is not connected, don't bother to check it
                    if (!padStates [i].IsConnected) continue;
                    // Check every button on the controller for events
                    for (int b = 0; b < buttons.Length; b++)
                    {
                        Buttons button = (Buttons) buttons.GetValue(b);
                        if (padStates [i].IsButtonDown(button) && prevPadStates [i].IsButtonUp(button))
                            inputProccessor.ButtonDown(playerIndexes [i], button);
                        if (padStates [i].IsButtonUp(button) && prevPadStates [i].IsButtonDown(button))
                            inputProccessor.ButtonUp(playerIndexes [i], button);
                    }
                }
            }

            // Switch the keyboard, mouse and pad states around
            prevKeyboardState = keyboardState;
            prevMouseState = mouseState;
            for (int c = 0; c < padStates.Length; c++)
            {
                prevPadStates [c] = padStates [c];
            }
        }

        /// <summary>
        /// Sends a mouse click event to the input proccessor and sets the MouseButton pressed flag to true
        /// This also automatically starts a dragging events, although the event is not sent to the input proccessor until
        /// the X and Y coordinates are different from that of the click down event
        /// </summary>
        /// <param name="button"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SendMousePressed(MouseButton button, float x, float y)
        {
            inputProccessor.MousePressed(button, (int) x, (int) y);
            if (dragging) return;
            mouseDragged.X = mouse.X;
            mouseDragged.Y = mouse.Y;
            draggingButton = button;
            dragging = true;
        }

        /// <summary>
        /// Sends a mouse up event to the input proccessor and sets the MouseButton pressed flag to false
        /// </summary>
        /// <param name="button"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void SendMouseReleased(MouseButton button, float x, float y)
        {
            inputProccessor.MouseReleased(button, (int) x, (int) y);
            if (button == draggingButton)
                dragging = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True if the given key is down</returns>
        public bool IsKeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns>True if the given key is released</returns>
        public bool IsKeyUp(Keys key)
        {
            return keyboardState.IsKeyUp(key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="button"></param>
        /// <returns>True if the given mouse button is down</returns>
        public bool IsMouseButtonDown(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.LEFT:
                    return mouseButtonStates [0] == PRESSED && prevMouseButtonStates[0] == RELEASED;
                case MouseButton.MIDDLE:
                    return mouseButtonStates[1] == PRESSED && prevMouseButtonStates[1] == RELEASED;
                case MouseButton.RIGHT:
                    return mouseButtonStates[2] == PRESSED && prevMouseButtonStates[2] == RELEASED;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="button"></param>
        /// <returns>True if the given mouse button is up</returns>
        public bool IsMouseButtonUp(MouseButton button)
        {
            switch (button)
            {
                case MouseButton.LEFT:
                    return mouseButtonStates [0] == RELEASED && prevMouseButtonStates[0] == PRESSED;
                case MouseButton.MIDDLE:
                    return mouseButtonStates[1] == RELEASED && prevMouseButtonStates[1] == PRESSED;
                case MouseButton.RIGHT:
                    return mouseButtonStates[2] == RELEASED && prevMouseButtonStates[2] == PRESSED;
            }
            return false;
        }

        /// <summary>
        /// Transforms the mouse coordinates to world coordinates using a set matrix
        /// </summary>
        /// <returns>The mouse position vector</returns>
        public Vector2 MouseToWorld()
        {
            return Vector2.Transform(new Vector2(mouse.X, mouse.Y), Matrix.Invert(transformMatrix));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The mouse position on the X axis</returns>
        public float GetMouseX()
        {
            return mouse.X;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The mouse position on the Y axis</returns>
        public float GetMouseY()
        {
            return mouse.Y;
        }

        public Matrix TransformMatrix
        {
            get { return transformMatrix; }
            set { transformMatrix = value; }
        }

        /// <summary>
        /// Gets or sets the position of the mouse
        /// </summary>
        public Vector2 MousePosition
        {
            get { return mouse; }
            set { mouse = value; }
        }

        public Vector2 MouseDragged
        {
            get { return mouseDragged; }
            set { mouseDragged = value; }
        }

        public IInputProccessor InputProccessor
        {
            get { return inputProccessor; }
            set { inputProccessor = value; }
        }

        public bool AnyKeyPressed()
        {
            return pressedKeys.Length > 0;
        }
    }
}