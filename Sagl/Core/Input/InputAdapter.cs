﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sagl.Interfaces;

// Author : Stephen Gibson

namespace Sagl.Core.Input
{
    /// <summary>
    /// Allows a class to only implement the methods neccessary from the input proccessor
    /// <see cref="IInputProccessor"/>
    /// </summary>
    public class InputAdapter : IInputProccessor
    {
        public virtual void PollInput(GameTime gameTime) {}

        public virtual void KeyDown(Keys key) {}

        public virtual void KeyUp(Keys key) {}

        public virtual void MousePressed(InputHandler.MouseButton button, int x, int y) {}

        public virtual void MouseReleased(InputHandler.MouseButton button, int x, int y) {}

        public virtual void MouseDragged(InputHandler.MouseButton button, int endX, int endY) {}

        public virtual void MouseMoved(int x, int y) {}

        public virtual void Scrolled(int direction) {}

        public virtual void ButtonDown(PlayerIndex index, Buttons button) {}

        public virtual void ButtonUp(PlayerIndex index, Buttons button) {}
    }
}