﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Sagl.Interfaces;

// Author : Stephen Gibson

namespace Sagl.Core.Input
{
    /// <summary>
    /// The InputMultiplexer allows the InputHandler to send events to many InputProccessors at once,
    /// e.g UI input and Gameplay input
    /// This works in a first in last out algorithm, like a standard list. InputProccessors added first will be polled first
    /// </summary>
    public class InputMultiplexer : List<IInputProccessor>, IInputProccessor
    {
        /// <summary>
        /// Creates a new, empty input multiplexer
        /// </summary>
        public InputMultiplexer() {}

        /// <summary>
        /// Creates a new input multiplexer with the given input proccessors
        /// </summary>
        /// <param name="proccessors"></param>
        public InputMultiplexer(params IInputProccessor[] proccessors)
        {
            AddRange(proccessors);
        }

        public void PollInput(GameTime gameTime)
        {
            foreach (IInputProccessor i in this)
            {
                i.PollInput(gameTime);
            }
        }

        public void KeyDown(Keys key)
        {
            foreach (IInputProccessor i in this)
            {
                i.KeyDown(key);
            }
        }

        public void KeyUp(Keys key)
        {
            foreach (IInputProccessor i in this)
            {
                i.KeyUp(key);
            }
        }

        public void MousePressed(InputHandler.MouseButton button, int x, int y)
        {
            foreach (IInputProccessor i in this)
            {
                i.MousePressed(button, x, y);
            }
        }

        public void MouseReleased(InputHandler.MouseButton button, int x, int y)
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i].MouseReleased(button, x, y);
            }
            
            /*foreach (IInputProccessor i in this)
            {
                i.MouseReleased(button, x, y);
            }*/ // note: taken out as collection was modified during runtime
        }

        public void MouseDragged(InputHandler.MouseButton button, int endX, int endY)
        {
            foreach (IInputProccessor i in this)
            {
                i.MouseDragged(button, endX, endY);
            }
        }

        public void MouseMoved(int x, int y)
        {
            foreach (IInputProccessor i in this)
            {
                i.MouseMoved(x, y);
            }
        }

        public void Scrolled(int direction)
        {
            foreach (IInputProccessor i in this)
            {
                i.Scrolled(direction);
            }
        }

        public void ButtonDown(PlayerIndex index, Buttons button)
        {
            foreach (IInputProccessor i in this)
            {
                i.ButtonDown(index, button);
            }
        }

        public void ButtonUp(PlayerIndex index, Buttons button)
        {
            foreach (IInputProccessor i in this)
            {
                i.ButtonUp(index, button);
            }
        }
    }
}