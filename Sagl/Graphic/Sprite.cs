﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sagl.Interfaces;
using Sagl.Math;

// Author : Stephen Gibson

namespace Sagl.Graphic
{
    /// <summary>
    /// Sprite for drawing to the screen, each sprite holds a reference to a texture and manipulates said
    /// texture in the batch call. Sprites are resizable, can be directly scaled and colored. 
    /// A sprite can be directly passed to a renderer for drawing.
    /// </summary>
    public class Sprite : Interfaces.IDrawable, IShader
    {
        // The textue this sprite is using
        protected Texture2D texture;

        // The transform for this sprite, position, origin, scale and rotation
        protected Transform2D transform;

        // The width and height of this sprite, indepedent from scale
        protected float width, height;

        // Internal scale for the sprite, for setting the "size"
        protected Vector2 sizeScale;

        // The color of this sprite
        protected Color color;
        // The depth layer for this sprite
        protected float layer;
        // The flip effect for the sprite
        protected SpriteEffects effect;

        // The shader to apply to the texture for this sprite
        protected Shader shader;

        /// <summary>
        /// Creates a textureless sprite with the default values
        /// </summary>
        public Sprite()
        {
            SetDefaults();
        }

        /// <summary>
        /// Creates a new sprite with the given texture and a default transform, color, layer and effect
        /// </summary>
        /// <param name="texture">The texture to be drawn</param>
        public Sprite(Texture2D texture)
            : this()
        {
            width = texture.Width;
            height = texture.Height;
            SetTexture(texture);
        }

        /// <summary>
        /// Creats a new sprite from an existing sprite, using the same texture
        /// </summary>
        /// <param name="sprite">The sprite to take the texture from</param>
        public Sprite(Sprite sprite) : this(sprite.GetTexture()) {}

        /// <summary>
        /// Creates a new sprite with the given texture and transform reference, 
        /// as well as it's default color and layer it should be rendered on
        /// </summary>
        /// <param name="texture">The texture to be drawn</param>
        /// <param name="color">The default color of the sprite</param>
        /// <param name="layer">What layer this sprite should be drawn on, ranged from 0-1</param>
        public Sprite(Texture2D texture, Color color, float layer)
            : this(texture)
        {
            this.color = color;
            this.layer = layer;
        }

        /// <summary>
        /// Creates a new sprite with the given texture, position, origin, 
        /// scale and rotation; also the default color and layer it should be rendered on
        /// </summary>
        /// <param name="texture">The texture to be drawn</param>
        /// <param name="position">The position of the sprite in world coordinates</param>
        /// <param name="origin">The origin of the sprite relative to the position</param>
        /// <param name="scale">The scale of the sprite, 1 being normal, 0.5 being half a 2 being double</param>
        /// <param name="rotation">The rotation of the sprite in radians</param>
        /// <param name="color">The default color of the sprite</param>
        /// <param name="layer">What layer this sprite should be drawn on, ranged from 0-1</param>
        public Sprite(Texture2D texture, Vector2 position, Vector2 origin, Vector2 scale, float rotation,
            Color color,
            float layer) : this(texture, color, layer)
        {
            transform = new Transform2D();
            transform.SetPosition(position.X, position.Y);
            transform.SetScale(scale.X, scale.Y);
            transform.SetAngle(rotation);
            transform.origin = origin;
        }


        /// <summary>
        /// Draws this sprite using it's transform, effect, shader and depth layer
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="gameTime"></param>
        public virtual void Draw(SpriteBatch batch, GameTime gameTime)
        {
            // Sprites can have a null state, as they might not have a texture assigned at "that time", therefore any
            // null textures should be ignored and not caught as an exception
            if (texture == null) return;
            batch.Draw(texture, transform.GetPosition(), null, color, transform.GetRotationRad(), transform.origin,
                sizeScale * transform.GetScale(),
                effect, layer);
        }

        /// <summary>
        /// Sets the default values for the sprite
        /// </summary>
        private void SetDefaults()
        {
            transform = new Transform2D();
            sizeScale = Vector2.One;
            effect = SpriteEffects.None;
            color = Color.White;
            layer = 1;
        }

        /// <summary>
        /// Sets the current texture for this sprite
        /// </summary>
        /// <param name="texture">The new texture to set</param>
        public void SetTexture(Texture2D texture)
        {
            if (texture == null)
                throw new ArgumentNullException("texture", "Set texture can not be null");
            this.texture = texture;
            float oldWidth = width;
            float oldHeight = height;
            width = texture.Width;
            height = texture.Height;
            SetSize(oldWidth, oldHeight);
        }

        /// <summary>
        /// Sets the size of this sprite to a specified value, expressed in whatever units
        /// </summary>
        /// <param name="width">The new width of the sprite</param>
        /// <param name="height">The new height of the sprite</param>
        public void SetSize(float width, float height)
        {
            if(MathUtils.FuzzyEpsilonEquals(width, 0, 0.00005f) || MathUtils.FuzzyEpsilonEquals(height, 0, 0.00005f)) return;
            sizeScale.X = width / this.width;
            sizeScale.Y = height / this.height;
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Sets the color of the sprite
        /// </summary>
        /// <param name="color">The new color</param>
        public void SetColor(Color color)
        {
            this.color = color;
        }

        /// <summary>
        /// Gets the color of the sprite
        /// </summary>
        /// <returns></returns>
        public Color GetColor()
        {
            return color;
        }

        /// <summary>
        /// Sets the position of this sprite given the X and Y values
        /// </summary>
        /// <param name="x">The new X coordinate of the sprite</param>
        /// <param name="y">The new Y coordinate of the sprite</param>
        public void SetPosition(float x, float y)
        {
            transform.SetPosition(x, y);
        }

        /// <summary>
        /// Sets the transform of this sprite to that of another
        /// </summary>
        /// <param name="transform">The reference transform</param>
        public void SetTransform(Transform2D transform)
        {
            this.transform = transform;
        }

        public Transform2D GetTransform()
        {
            return transform;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position of the sprite as a Vector2</returns>
        public Vector2 GetPosition()
        {
            return transform.GetPosition();
        }

        /// <summary>
        /// Sets the scale of this sprite on all axis
        /// </summary>
        /// <param name="scale"></param>
        public void SetScale(float scale)
        {
            SetScale(scale, scale);
        }

        /// <summary>
        /// Sets the scale of this sprite on the X and Y axis indepedently
        /// </summary>
        /// <param name="scaleX"></param>
        /// <param name="scaleY"></param>
        public void SetScale(float scaleX, float scaleY)
        {
            transform.SetScale(scaleX, scaleY);
        }

        /// <summary>
        /// Sets the Z-Buffer layer of this sprite
        /// </summary>
        /// <param name="layer">Value between 0 and 1</param>
        public void SetLayer(float layer)
        {
            this.layer = layer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The layer the sprite is rendered on</returns>
        public float GetLayer()
        {
            return layer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position of this sprites X coordinate</returns>
        public float GetX()
        {
            return transform.GetPosition().X;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position of this sprits Y coordinate</returns>
        public float GetY()
        {
            return transform.GetPosition().Y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The width of the sprite independent of scaling</returns>
        public virtual float GetWidth()
        {
            return width;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The height of the sprite indepedent of scaling</returns>
        public virtual float GetHeight()
        {
            return height;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The scale of the sprite on the X coordinate</returns>
        public float GetScaleX()
        {
            return transform.GetScale().X;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The scale of the sprite on the Y coordinate</returns>
        public float GetScaleY()
        {
            return transform.GetScale().Y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The texture this sprite has set for drawing</returns>
        public Texture2D GetTexture()
        {
            return texture;
        }

        /// <summary>
        /// Sets the shader effect for this sprite, this is then used to apply to the texture for the sprite
        /// </summary>
        /// <param name="shader"></param>
        public void SetShader(Shader shader)
        {
            this.shader = shader;
        }

        /// <summary>
        /// Gets the shader for this sprites texture
        /// </summary>
        /// <returns></returns>
        public Shader GetShader()
        {
            return shader;
        }

        public virtual void SetEnabled(bool state)
        {
        }

        public virtual bool IsEnabled()
        {
            return true;
        }
    }
}