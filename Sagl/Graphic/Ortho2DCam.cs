﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sagl.Math;
using Sagl.Utils;
using Game = Sagl.Core.Game;

// Author : Stephen Gibson

namespace Sagl.Graphic
{
    /// <summary>
    /// Repesents a scalable camera projected onto a 2D orthograpic plane.
    /// </summary>
    public class Ortho2DCam
    {
        // The position of the camera in whatever coordinates are used
        private Vector3 position;
        private float zoom;

        // World matrix for position and scale of the camera
        private Matrix world;
        // The view, projection and world matrix combined
        private Matrix combined;

        // The scale of the camera, for internal matrix scaling
        private float scaleX, scaleY;
        // The width and height of the camera in units
        private float width, height;
        // The origin of the camera
        private Vector3 origin;

        // The viewport of the game, not the camera
        private Viewport viewport;

        // The bounds of the cameras viewport
        private Bounds camBounds;
        // The bounds of the world the camera should adhere to
        private Bounds worldBounds;
        // These widths and heights are used to determine the boundary while zoomed
        private float bWidth, bHeight;

        // If camera shake is enabled
        private bool shakeEnabled;
        // If the camera is currently shaking
        private bool shaking;
        // The total time the camera will shake for
        private float shakeTime;
        // The current shake time of the camera
        private float currentShakeTime;
        // The power of the shake
        private float shakePower;
        // The current shake power
        private float currentShakePower;
        // How much the camera should move on X
        private float shakeX;
        // How much the camera should shake on Y
        private float shakeY;
        // The camera positon prior to shake
        private float x1, y1, z1;
        // The camera position after shake
        private float x2, y2, z2;

        // The smoothing between the cameras old and new position
        private float smoothing = 1;


        /// <summary>
        /// Creats a new 2D ortho camera positioned at 0,0 with the width and height equal to that of the viewport
        /// </summary>
        /// <param name="viewport">The viewport of the window instance</param>
        public Ortho2DCam(Viewport viewport)
        {
            this.viewport = viewport;
        }

        /// <summary>
        /// Create a new 2D ortho camera with a given position, width and height
        /// </summary>
        /// <param name="viewport">The viewport of the window instance</param>
        /// <param name="width">The width of the camera in world space</param>
        /// <param name="height">The height of the camera in world space</param>
        /// <param name="position">The position of the camera in world space</param>
        public Ortho2DCam(Viewport viewport, float width, float height, Vector2 position) : this(viewport)
        {
            if (width <= 0 || height <= 0)
                throw new ArgumentException("" + (width <= 0 ? "Width" : "Height") +
                                            " can not be less or equal to zero " + width + "x" + "height");
            scaleX = viewport.Width / width;
            scaleY = viewport.Height / height;
            this.width = width;
            this.height = height;
            bWidth = width;
            bHeight = height;
            this.position = new Vector3(position, 0);
            camBounds = new Bounds(position.X, position.Y, width, height);
            zoom = 1;
            // Create the camera matrices
            Update(Game.GetGameTime());
            x1 = position.X;
            x2 = position.X;
            y1 = position.Y;
            y2 = position.Y;
        }

        /// <summary>
        /// Create a new 2D ortho camera with a given position
        /// </summary>
        /// <param name="viewport">The viewport of the window instance</param>
        /// <param name="width">The width of the camera in world space</param>
        /// <param name="height">The height of the camera in world space</param>
        /// <param name="x">The X position in world space</param>
        /// <param name="y">The Y position in world space</param>
        public Ortho2DCam(Viewport viewport, float width, float height, float x, float y)
            : this(viewport, width, height, new Vector2(x, y)) {}

        /// <summary>
        /// Create a new 2D ortho camera with a given position and world bounds
        /// </summary>
        /// <param name="viewport">The viewport of the window instance</param>
        /// <param name="width">The width of the camera in world space</param>
        /// <param name="height">The height of the camera in world space</param>
        /// <param name="position">The position in world space</param>
        /// <param name="worldBounds">The boundaries of the world edges</param>
        public Ortho2DCam(Viewport viewport, float width, float height, Vector2 position, Bounds worldBounds)
            : this(viewport, width, height, position)
        {
            this.worldBounds = worldBounds;
        }


        /// <summary>
        /// Create a new 2D ortho camera with a given position and world bounds
        /// </summary>
        /// <param name="viewport">The viewport of the window instance</param>
        /// <param name="width">The width of the camera in world space</param>
        /// <param name="height">The height of the camera in world space</param>
        /// <param name="x">The X position in world space</param>
        /// <param name="y">The Y position in world space</param>
        /// <param name="worldBounds">The boundaries of the world edges</param>
        public Ortho2DCam(Viewport viewport, float width, float height, float x, float y, Bounds worldBounds)
            : this(viewport, width, height, new Vector2(x, y), worldBounds) {}


        /// <summary>
        /// Updates the camera each frame, updates the matrices with the position and scale of the camera
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            origin = new Vector3((viewport.Width / 2f), (viewport.Height / 2f), 0);
            // Fuck the rules
            world = Matrix.CreateTranslation(-position.X, -position.Y, 0) *
                    Matrix.CreateScale(scaleX * zoom, scaleY * zoom, 1) *
                    Matrix.CreateTranslation(origin.X, origin.Y, 0);
            combined *= world;

            position.X = MathHelper.Lerp(x1, x2, smoothing);
            position.Y = MathHelper.Lerp(y1, y2, smoothing);

            if(shaking)
                ProcessShake(gameTime);
                
        }

        /// <summary>
        /// Starts a camera shake with the given power, time and smoothing factor
        /// </summary>
        /// <param name="power">How rigerious the camera movement will be</param>
        /// <param name="time">How long in ms the camera should shake for</param>
        /// <param name="smoothing">How smooth the camera should transist between positions</param>
        public void Shake(float power, float time, float smoothing)
        {
            if(!shakeEnabled) return;
            shakePower = power;
            shakeTime = time;
            this.smoothing = smoothing;
            currentShakeTime = 0;
            x1 = position.X;
            y2 = position.Y;
            shaking = true;
        }

        /// <summary>
        /// Shakes the camera
        /// </summary>
        /// <param name="gameTime"></param>
        private void ProcessShake(GameTime gameTime)
        {
            /*
            * If the current shake time is less than the desired shake time, shake
            * away
            */
            if (currentShakeTime <= shakeTime)
            {
                currentShakePower = shakePower * ((shakeTime - currentShakeTime / 1.15f) / shakeTime);
                /*
                * Randomly increase/decrease the position on x by a random factor
                * multiplied by the shakepower
                */
                shakeX = (MathUtils.Random(-0.5f, 0.5f) * MathUtils.Random(1.25f,
                        1.5f)) * currentShakePower;
                /*
                 * Randomly increase/decrease the position on y by a random factor
                 * multiplied by the shakepower
                 */
                shakeY = (MathUtils.Random(-0.5f, 0.5f) * MathUtils.Random(1.25f,
                        1.5f)) * currentShakePower;

                /* Move the camera to the new values */
                Translate(-shakeX, -shakeY);
                /* increase our current shake time */
                currentShakeTime += gameTime.ElapsedGameTime.Milliseconds;
            }
            else
            {
                x2 = position.X;
                y2 = position.Y;
                shaking = false;
            }
        }

        /// <summary>
        /// Toggles the camera shake on and off
        /// </summary>
        public void ToggleShake()
        {
            shakeEnabled = !shakeEnabled;
        }

        /// <summary>
        /// Moves the camera along the X and Y axis using the given values, this manipulates the cameras
        /// position relative to where it is already located
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void Translate(float x, float y)
        {
            position.X += x;
            position.Y += y;
            if (worldBounds == null) return;
            KeepInBounds();
        }

        /// <summary>
        /// Moves the camera along the given X and Y axis using the specified vector3, 
        /// this manipulates the cameras position relative to where it is already located
        /// </summary>
        /// <param name="translation"></param>
        public void Translate(Vector3 translation)
        {
            Translate(translation.X, translation.Y);
        }

        /// <summary>
        /// Sets the position of the camera directly using the given X and Y
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetPosition(float x, float y)
        {
            position.X = x;
            position.Y = y;
            KeepInBounds();
        }

        /// <summary>
        /// Sets the position of the camera directly using the given vector2
        /// </summary>
        /// <param name="position"></param>
        public void SetPosition(Vector2 position)
        {
            SetPosition(position.X, position.Y);
        }

        /// <summary>
        /// Zooms the camera with a given amount, zoom is represented between 0 to n, where n is the 
        /// current zoom level. Default 1, min value 0 + mininum amount.
        /// </summary>
        /// <param name="amount"></param>
        public void Zoom(float amount)
        {
            // If the zoom is less going to be less than zero, return
            if (zoom + amount <= 0) return;
            zoom += amount;
            bWidth = width / zoom;
            bHeight = height / zoom;
            // Keep the camera in bounds
            KeepInBounds();
        }

        /// <summary>
        /// Keeps the camera position in bounds with the world boundary
        /// </summary>
        private void KeepInBounds()
        {
            const float OFFSET = 0.00001f;
            float w = ((bWidth / 2));
            float h = ((bHeight / 2));
            if (position.X < w)
                position.X = w + OFFSET;
            if (position.X > worldBounds.GetWidth() - w)
                position.X = worldBounds.GetWidth() - w - OFFSET;
            if (position.Y < h)
                position.Y = h + OFFSET;
            if (position.Y > worldBounds.GetHeight() - h)
                position.Y = worldBounds.GetHeight() - h - OFFSET;

            // Resize the cameras boundary to match the cameras new viewport size
            camBounds.SetBoundarySize(bWidth, bHeight);
            // Keep the camera bounds to that of the position of the actual camera
            camBounds.SetPosition(position.X, position.Y);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position of the camera on the X coordinate</returns>
        public float GetX()
        {
            return position.X;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position of the camera on the Y coordinate</returns>
        public float GetY()
        {
            return position.Y;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The viewport of the window, <b>NOT</b> camera</returns>
        public Viewport GetViewport()
        {
            return viewport;
        }

        /// <summary>
        /// Gets or sets the position of the camera instantly
        /// </summary>
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Gets the zoom level ranged from 0 to n
        /// </summary>
        /// <returns></returns>
        public float GetZoom()
        {
            return zoom;
        }

        /// <summary>
        /// Gets the width of the camera viewport
        /// </summary>
        /// <returns></returns>
        public float GetWidth()
        {
            return width;
        }

        /// <summary>
        /// Gets the height of the camera viewport
        /// </summary>
        /// <returns></returns>
        public float GetHeight()
        {
            return height;
        }

        /// <summary>
        /// Gets the boundary of the camera
        /// </summary>
        /// <returns></returns>
        public Bounds GetCameraBounds()
        {
            return camBounds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The world, projection and view matrices multiplied into one</returns>
        public Matrix Combined()
        {
            return combined;
        }

        /// <summary>
        /// Gets the world matrix
        /// </summary>
        public Matrix World
        {
            get { return world; }
        }
    }
}