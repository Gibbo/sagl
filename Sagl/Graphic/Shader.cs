﻿using System;
using Microsoft.Xna.Framework.Graphics;

// Author : Stephen Gibson

namespace Sagl.Graphic
{
    /// <summary>
    /// Represents a data structure for a shader. 
    /// This includes the effect to apply, the technique to use and the pass to apply.
    /// </summary>
    public class Shader
    {
        // The effect for the shader
        private Effect effect;
        // The technique to use
        private int technique;
        // The pass to use
        private int pass;

        /// <summary>
        /// Create a new shader with the given effect
        /// </summary>
        /// <param name="effect">The effect to use for the shader</param>
        public Shader(Effect effect)
        {
            this.effect = effect;
            SetPass(0);
            SetTechnique(0);
        }

        /// <summary>
        /// Create a new shader with the given path to a shader file.
        /// This method will always load a new effect from disc.
        /// </summary>
        /// <param name="file">The path to the file</param>
        public Shader(String file) : this((Effect) Game.instance.Content.Load<Effect>(file)) {}

        /// <summary>
        ///  Applies the shader with the data
        /// </summary>
        public void Apply()
        {
            effect.Techniques [technique].Passes [pass].Apply();
        }

        /// <summary>
        /// Sets the pass to use for the shader
        /// </summary>
        /// <param name="pass"></param>
        /// <returns>This instance for chaining</returns>
        public Shader SetPass(int pass)
        {
            this.pass = pass;
            return this;
        }

        /// <summary>
        /// Sets the technique to use for the shader
        /// </summary>
        /// <param name="technique"></param>
        /// <returns>This instance for chaining</returns>
        public Shader SetTechnique(int technique)
        {
            this.technique = technique;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>A collection of all the effects parameters for getting and setting</returns>
        public EffectParameterCollection GetParameters()
        {
            return effect.Parameters;
        }

        public Effect GetEffect()
        {
            return effect;
        }
    }
}