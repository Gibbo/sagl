﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

// Author : Stephen Gibson

namespace Sagl.Graphic
{
    /// <summary>
    /// A simple delegate that is called whenever the animation is finished
    /// </summary>
    public delegate void OnFinish();

    public class AnimatedSprite : Sprite
    {
        /// <summary>
        /// Defines the play mode for the animation, such as normal, loop, reverse etc
        /// </summary>
        public enum PlayMode
        {
            NORMAL,
            LOOP,
            REVERSED_NORMAL,
            REVERSED_LOOP
        }

        // If the animation is playing
        private bool isPlaying = true;

        // The current mode of play for the animation
        private PlayMode playMode = PlayMode.NORMAL;
        // The current frame number
        public int frameNumber = 0;
        // The current frame the animation is on
        private Rectangle frame;
        // The frames that have been Split from the texture
        private Rectangle[] frames;
        // The current state time or "frame time" of the animation
        private float stateTime;
        // The total duration of the animation
        private float animationDuration;
        // The current time passed for the entire animation
        private float totalTimePassed;
        // The total duration of each frame
        private float frameDuration;
        // number of rows in the animation
        private int rows;
        // number of columns in the animation
        private int cols;

        // For when the animation is finished
        private OnFinish onFinish;

        /// <summary>
        /// Creates a new animation with the default PlayMode Normal and sets it to playing
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <param name="frameDuration"></param>
        public AnimatedSprite(Texture2D spriteSheet, int rows, int cols, float frameDuration)
        {
            SetTexture(spriteSheet);
            frames = Create(spriteSheet, rows, cols);
            this.rows = rows;
            this.cols = cols;
            this.frameDuration = frameDuration;
            animationDuration = frameDuration * frames.Length;
            frame = frames [0];
            Play();
        }

        /// <summary>
        /// Creates a new animation with the given Play mode and sets it to playing
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <param name="frameDuration"></param>
        /// <param name="playMode"></param>
        public AnimatedSprite(Texture2D spriteSheet, int rows, int cols, float frameDuration, PlayMode playMode)
            : this(spriteSheet, rows, cols, frameDuration)
        {
            this.playMode = playMode;
            if (playMode == PlayMode.REVERSED_LOOP || playMode == PlayMode.REVERSED_NORMAL)
                frameNumber = frames.Length - 1;
        }

        /// <summary>
        /// Creates a new animation, this constructor is used for play once animations. Once the animation is finished
        /// it will invoke the given delegate. Useful for death animations where the owner of the animation has to be destroyed
        /// 
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <param name="frameDuration"></param>
        /// <param name="onFinish"></param>
        public AnimatedSprite(Texture2D spriteSheet, int rows, int cols, float frameDuration, OnFinish onFinish)
            : this(spriteSheet, rows, cols, frameDuration)
        {
            playMode = PlayMode.NORMAL;
            this.onFinish = onFinish;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            // Update the current frames state time and the total time in the animation
            stateTime += gameTime.ElapsedGameTime.Milliseconds;
            totalTimePassed += gameTime.ElapsedGameTime.Milliseconds;

            // If the current frames state time has not exceeded the set frame duration, return
            if (!(stateTime > frameDuration)) return;

            // If the animation is finished we want to either replay it or stop it
            if (IsFinished())
            {
                // Check if play mode is in a loop
                if (playMode == PlayMode.LOOP || playMode == PlayMode.REVERSED_LOOP)
                {
                    // Set current frame number depending on if we are in reverse or normal loop
                    frameNumber = playMode == PlayMode.LOOP ? 0 : frames.Length - 1;
                    frame = frames [frameNumber];
                    totalTimePassed = 0;
                }
                else
                {
                    Stop();
                    // If the animation is finished and we have a delegate to call, call it
                    if (onFinish == null) return;
                    onFinish.Invoke();
                }
            }
            else
            {
                // Animation is not finished so just update the frame number and the current frame
                frameNumber += playMode == PlayMode.LOOP || playMode == PlayMode.NORMAL ? 1 : -1;
                frame = frames [frameNumber];
            }
            // Reset state time
            stateTime = 0;
        }

        public override void Draw(SpriteBatch batch, GameTime gameTime)
        {
            if(isPlaying)
                Update(gameTime);
            batch.Draw(texture, transform.GetPosition(), frame, color, transform.GetRotationRad(), transform.origin,
                sizeScale * transform.GetScale(), effect, layer);
        }

        /// <summary>
        /// Creates the animation by splitting the texture into regions
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        private Rectangle[] Create(Texture2D spriteSheet, int rows, int cols)
        {
            // Actual frames
            Rectangle[] frames = new Rectangle[cols * rows];
            // 2D array in from splitting the spritesheet, split it here
            Rectangle[,] regions = Split(spriteSheet, rows, cols);
            int index = 0;
            // Simply populate the frames array from regions we just split
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    frames [index] = regions [row, col];
                    index++;
                }
            }
            return frames;
        }

        /// <summary>
        /// Splits the sprite sheet into separate frames
        /// </summary>
        /// <param name="spriteSheet"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        private Rectangle[,] Split(Texture2D spriteSheet, int rows, int cols)
        {
            int x = 0;
            int y = 0;
            int width = spriteSheet.Width;
            int height = spriteSheet.Height;

            int frameHeight = height / rows;
            int frameWidth = width / cols;

            // The regions that will come from the split, this is what is returned in the end
            Rectangle[,] regions = new Rectangle[rows, cols];

            // Take every row and column and create a rectanle from it given the frame height and width
            for (int row = 0; row < rows; row++, y += frameHeight)
            {
                for (int col = 0; col < cols; col++, x += frameWidth)
                {
                    regions [row, col] = new Rectangle(x, y, frameWidth, frameHeight);
                }
            }
            return regions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>If the animation has finished</returns>
        public bool IsFinished()
        {
            return totalTimePassed >= animationDuration;
        }

        /// <summary>
        /// Plays the animation, this will reset the animation back to the first frame.
        /// </summary>
        public void Play()
        {
            isPlaying = true;
            Reset();
        }

        /// <summary>
        /// Stops the animation and resets it back to the first frame
        /// </summary>
        public void Stop()
        {
            isPlaying = false;
            Reset();
        }

        /// <summary>
        /// Pauses the animation at the current frame/time
        /// </summary>
        public void Pause()
        {
            isPlaying = false;
        }


        /// <summary>
        /// Resumes the animation from a paused state
        /// </summary>
        public void Resume()
        {
            isPlaying = true;
        }


        /// <summary>
        /// Resets the animations stateTime and totalTimePassed back to 0 and the frame back to the first
        /// </summary>
        private void Reset()
        {
            stateTime = 0;
            totalTimePassed = 0;
            frameNumber = playMode == PlayMode.NORMAL || playMode == PlayMode.LOOP ? 0 : frames.Length - 1;
            frame = frames[frameNumber];
        }

        public bool IsPlaying()
        {
            return isPlaying;
        }

        /// <summary>
        /// Gets or sets the current statetime of the current frame, basically how long it has been running for
        /// </summary>
        public float StateTime
        {
            get { return stateTime; }
            set { stateTime = value; }
        }

        public override float GetWidth()
        {
            return base.GetWidth() / cols;
        }

        public override float GetHeight()
        {
            return base.GetHeight() / rows;
        }

        public void SetOnFinish(OnFinish onFinish)
        {
            this.onFinish = onFinish;
        }
    }
}