﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sagl.Interfaces;
using IDrawable = Sagl.Interfaces.IDrawable;

// Author : Stephen Gibson

namespace Sagl.Graphic
{
    /// <summary>
    /// The renderer abstracts draw code away making it easier to manage, each renderer instance
    /// holds its own spritebatch and all other properties required to batch sprites (sort mode, texture filter etc)
    /// </summary>
    public class Renderer : List<IDrawable>, IObserver
    {
        // Sprite batch for rendering drawables
        private SpriteBatch batch;
        // The projection matrix for rendering drawables
        private Matrix projection;

        // Sort mode for ordering the sprites when batching
        private SpriteSortMode sortMode;

        // Sampler state for setting Texture Filtering
        private SamplerState textureFilter;

        // The current pixel shader to apply
        private Effect effect;

        /// <summary>
        /// Creates a new renderer instance given the graphics context and properties
        /// </summary>
        /// <param name="graphicsDevice"></param>
        /// <param name="projection"></param>
        /// <param name="sortMode"></param>
        /// <param name="textureFilter"></param>
        public Renderer(GraphicsDevice graphicsDevice, Matrix projection, SpriteSortMode sortMode,
            SamplerState textureFilter)
        {
            Create(graphicsDevice);
            this.projection = projection;
            this.sortMode = sortMode;
            this.textureFilter = textureFilter;
        }

        /// <summary>
        /// Creates a new renderer instance given the graphics context. This automatically assigns the default 
        /// values:
        /// <code>Projection = Matrix.Identity</code>
        /// <code>SpriteSortMode = SpriteSortMode.BackToFront</code>
        /// <code>SamplerState = SamplerState.AnisostropicClamp</code>
        /// </summary>
        /// <param name="graphicsDevice"></param>
        public Renderer(GraphicsDevice graphicsDevice)
        {
            Create(graphicsDevice);
            SetDefaults();
        }

        /// <summary>
        /// Draws all drawables to the screen
        /// </summary>
        /// <param name="gameTime"></param>
        public void Render(GameTime gameTime)
        {
            batch.Begin(sortMode, BlendState.AlphaBlend, textureFilter, null, null, effect, projection);
            // Iterate over all drawables and draw them
            for (var i = 0; i < Count; i++)
            {
                // If the drawable is not enabled, don't draw it
                if (!this [i].IsEnabled()) continue;

                var shaderDrawable = this [i] as IShader;
                // Check if the drawable has a shader
                if (shaderDrawable != null)
                {
                    // Apply the shader
                    var shader = shaderDrawable.GetShader();
                    if (shader != null)
                        shader.Apply();
                }
                // Draw the object
                this [i].Draw(batch, gameTime);
            }
            batch.End();
        }

        /// <summary>
        /// Creates the sprite batch instance for the renderer
        /// </summary>
        /// <param name="graphicsDevice"></param>
        private void Create(GraphicsDevice graphicsDevice)
        {
            batch = new SpriteBatch(graphicsDevice);
        }

        /// <summary>
        /// Returns a copy of the render data
        /// </summary>
        /// <returns>The drawables in the form of a list</returns>
        public List<IDrawable> CopyRenderData()
        {
            return new List<IDrawable>(this);
        }

        /// <summary>
        /// Adds all the drawables from a given list to this renderer
        /// </summary>
        /// <param name="data"></param>
        /// <returns>This instance of the renderer for chaining</returns>
        public Renderer AddRenderData(List<IDrawable> data)
        {
            AddRange(data);
            return this;
        }

        /// <summary>
        /// Sets the default values for the renderer
        /// <code>Projection = Matrix.Identity</code>
        /// <code>SpriteSortMode = SpriteSortMode.BackToFront</code>
        /// <code>SamplerState = SamplerState.AnisostropicClamp</code>
        /// </summary>
        private void SetDefaults()
        {
            textureFilter = SamplerState.AnisotropicClamp;
            sortMode = SpriteSortMode.BackToFront;
            projection = Matrix.Identity;
        }

        /// <summary>
        /// Sets the mode for sprite sorting
        /// </summary>
        /// <param name="sortMode"></param>
        public void SetSpriteSortMode(SpriteSortMode sortMode)
        {
            this.sortMode = sortMode;
        }

        /// <summary>
        /// Sets the projection matrix for the batch
        /// </summary>
        /// <param name="projection"></param>
        public void SetProjectionMatrix(Matrix projection)
        {
            this.projection = projection;
        }

        /// <summary>
        /// Sets the texture filter for the batch
        /// </summary>
        /// <seealso cref="SamplerState"/>
        /// <param name="textureFilter"></param>
        public void SetTextureFilter(SamplerState textureFilter)
        {
            this.textureFilter = textureFilter;
        }

        /// <summary>
        /// Sets the shader effect for the <b>renderer</b>, this effect is applied to all drawables
        /// </summary>
        /// <param name="effect"></param>
        public void SetShaderEffect(Effect effect)
        {
            this.effect = effect;
        }

        /// <summary>
        /// Gives <b>all the drawables in the renderer</b> that supports shaders the given shader
        /// </summary>
        /// <param name="shader"></param>
        public void ApplyShaderToAll(Shader shader)
        {
            foreach (var drawable in this)
            {
                // If the drawable can use shaders, change its shader
                if (drawable is IShader)
                    ((IShader) drawable).SetShader(shader);
            }
        }

        /// <summary>
        /// Allows control of event driven renderer changes
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="objects"></param>
        public virtual void OnNotify(string tag, params object[] objects)
        {

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>The spritebatch the renderer is using</returns>
        public SpriteBatch GetBatch()
        {
            return batch;
        }
    }
}