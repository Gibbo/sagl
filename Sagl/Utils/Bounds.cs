﻿using Microsoft.Xna.Framework;

namespace Sagl.Utils
{
    /// <summary>
    /// Represents the boundaries and holds functions that tests if points are inside the boundary. Much like
    /// a rectangle but more defined
    /// </summary>
    public class Bounds
    {
        // The width and height of the boundary
        private float width;
        private float height;

        // The position of the bounds
        private Vector2 position;

        // The top left, bottom left, top right and bottom right points of the boundary
        public Vector2 tl;
        public Vector2 bl;
        public Vector2 tr;
        public Vector2 br;

        // If the boundary data has changed
        public bool changed;

        /// <summary>
        /// Creates a new boundary with the given position and size
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Bounds(float x, float y, float width, float height) : this(width, height)
        {
            SetPosition(x, y);
        }

        /// <summary>
        /// Creates a new boundary with the given width and height
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Bounds(float width, float height)
        {
            SetBoundarySize(width, height);
            changed = true;
        }

        /// <summary>
        /// Sets the size of the boundary and sets up all the corners
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SetBoundarySize(float width, float height)
        {
            this.width = width;
            this.height = height;
            SetCorners();
            changed = true;
        }

        /// <summary>
        /// Sets the position of the bounds
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetPosition(float x, float y)
        {
            position.X = x;
            position.Y = y;
        }


        public Vector2 GetPosition()
        {
            return position;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position on X</returns>
        public float GetX()
        {
            return position.X;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>The position on Y</returns>
        public float GetY()
        {
            return position.Y;
        }

        public float GetWidth()
        {
            return width;
        }

        public float GetHeight()
        {
            return height;
        }

        /// <summary>
        /// Checks if this bounds contains the given point
        /// </summary>
        /// <param name="point"></param>
        /// <returns>True if the point is inside the bounds</returns>
        public bool ContainsPoint(Vector2 point)
        {
            return ((point.X > 0 && point.X < width) && (point.Y > 0 && point.Y < height));
        }


        /// <summary>
        /// Sets the top left, bottom left, top right and bottom right points of the boundary
        /// </summary>
        private void SetCorners()
        {
            tl = new Vector2(0, height);
            bl = new Vector2(0, 0);
            tr = new Vector2(width, height);
            br = new Vector2(width, 0);
        }
    }
}
