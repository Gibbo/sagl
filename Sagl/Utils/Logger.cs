﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

// Author : Stephen Gibson

namespace Sagl.Utils
{
    public class Logger
    {
        public enum LogLevel
        {
            /// <summary>
            /// Only logs tagged with info will be logged
            /// </summary>
            INFO,

            /// <summary>
            /// Only logs tagged with degub will be logged
            /// </summary>
            DEBUG,

            /// <summary>
            /// All logs will be logged
            /// </summary>
            ALL
        }


        // The level of logging to output
        public static LogLevel logLevel = LogLevel.ALL;

        public static void Print(LogLevel logLevel, object message, string category)
        {
            if (logLevel == LogLevel.ALL || logLevel == Logger.logLevel)
                Trace.WriteLine(message, category);
        }

        public static void Print(LogLevel logLevel, object message)
        {
            if (logLevel == LogLevel.ALL || logLevel == Logger.logLevel)
                Trace.WriteLine(message.ToString());
        }
    }
}