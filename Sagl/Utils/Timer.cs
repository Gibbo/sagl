﻿using System;
using Microsoft.Xna.Framework;

namespace Sagl.Utils
{
    /// <summary>
    /// A basic timer class that triggers an event when a given amount of time has passed
    /// </summary>
    public class Timer
    {
        // If the timer is running
        private bool running;
        // If the timer is looping
        private bool isLooping;
        // The total time the timer should run for
        private float totalTime;
        // The current time the timer has ran for
        private float currentTime;
        // The action to execute when the timer is finished
        private Action onFinish;

        /// <summary>
        /// Creates a new timer
        /// </summary>
        /// <param name="time">How long the timer should run  in ms</param>
        /// <param name="loop">If the timer should run consistently</param>
        /// <param name="onFinish">What should occur when the timer is finished</param>
        public Timer(float time, bool loop, Action onFinish)
        {
            isLooping = loop;
            totalTime = time;
            this.onFinish = onFinish;
        }

        /// <summary>
        /// Updates the timer, should be called every game update for accurate timing
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            // Increment the current time with the time since last frame
            currentTime += gameTime.ElapsedGameTime.Milliseconds;
            // If the current time is less than total time nothing occurs
            if(currentTime < totalTime) return;

            // If there is an onFinish action, run it
            if(onFinish != null)
                onFinish.Invoke();

            // If the timer is looping, reset it
            if(isLooping)
                Reset();

        }

        /// <summary>
        /// Resets the timer back to 0 seconds but keeps the time, looping and onFinish parameters the same
        /// </summary>
        public void Reset()
        {
            Reset(totalTime, isLooping, onFinish);
        }

        /// <summary>
        /// Resets a timer with new parameters
        /// </summary>
        /// <param name="time">How long the timer should run  in ms </param>
        /// <param name="loop">If the timer should run consistently</param>
        /// <param name="onFinish"> What should occur when the timer is finished</param>
        public void Reset(float time, bool loop, Action onFinish)
        {
            totalTime = time;
            isLooping = loop;
            this.onFinish = onFinish;
            currentTime = 0;
        }

        /// <summary>
        /// Pauses the timer
        /// </summary>
        public void Pause()
        {
            running = false;
        }

        /// <summary>
        /// Resumes the timer from a paused state
        /// </summary>
        public void Resume()
        {
            running = true;
        }
    }
}
