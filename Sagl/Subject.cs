﻿using System;
using System.Collections.Generic;
using Sagl.Interfaces;

// Author : Stephen Gibson

namespace Sagl
{
    /// <summary>
    /// An subject is responsible for registration of observers. 
    /// Each registered observer is stored within a list and notified of events that occur.
    /// </summary>
    public class Subject
    {
        // All the observers that will be registered to this subject
        private List<IObserver> observers;
        // The max observers a subject may have registered, default 3
        private int max;

        /// <summary>
        /// Creates a new subject with the default max registered observers to 3
        /// </summary>
        public Subject() : this(3) {}

        /// <summary>
        /// Ceates a new subject with the specified max registered observers
        /// </summary>
        /// <param name="max">Specifies the max allowed observers, if -1 this number will be the max value of an unsigned 32 bit integer</param>
        public Subject(int max)
        {
            Init(max);
        }

        /// <summary>
        /// Initiliases the observers list with the max size as initial size
        /// </summary>
        /// <param name="max">The max amount of observers</param>
        private void Init(int max)
        {
            this.max = max == -1 ? int.MaxValue : max;
            observers = new List<IObserver>();
        }

        /// <summary>
        /// Registers a given observer to the subject, once registered this subject will be notify the observer
        /// of any events
        /// </summary>
        /// <param name="observer"></param>
        public Subject RegisterObserver(IObserver observer)
        {
            if (observers.Contains(observer)) throw new Exception("Observer already registered");
            if (observers.Count + 1 > max)
                throw new Exception("Subject can only register a max of '" + max + "' observers");
            observers.Add(observer);
            return this;
        }

        /// <summary>
        /// Unregisters a given observer from the subject, once unregistered this subject
        ///  will no longer notify that observer
        /// </summary>
        /// <param name="observer"></param>
        public void UnregisterObserver(IObserver observer)
        {
            if (!observers.Contains(observer)) throw new Exception("Observer not registered");
            observers.Remove(observer);
        }

        /// <summary>
        /// Notifies all the registered observers of the given event
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="objects"></param>
        public void Notify(String tag, params Object[] objects)
        {
            for (var i = 0; i < observers.Count; i++)
            {
                observers[i].OnNotify(tag, objects);
            }
        }
    }
}